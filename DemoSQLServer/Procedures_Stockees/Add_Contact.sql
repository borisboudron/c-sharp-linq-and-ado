﻿CREATE PROCEDURE [dbo].[Add_Contact]
	@nom NVARCHAR(50),
	@date DATETIME
AS
	SET NOCOUNT ON;
	INSERT INTO [dbo].[Contact] ([Name], [Birthdate]) VALUES (@nom, @date)
RETURN 0
