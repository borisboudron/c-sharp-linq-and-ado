﻿CREATE TABLE [dbo].[Contact]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Name] NVARCHAR(50) NOT NULL,
	[Birthdate] DATETIME NOT NULL,
    [Rdv] INT NULL DEFAULT NULL,
    [Active] BIT NOT NULL DEFAULT 1, 
    CONSTRAINT [CK_BirthDateValid] CHECK (Birthdate < GETDATE()),
    CONSTRAINT [FK_Contact_RendezVous] FOREIGN KEY ([Rdv]) REFERENCES [RendezVous]([Id])
)

GO

CREATE INDEX [IX_Contact_Name] ON [dbo].[Contact] ([Name])

GO

CREATE TRIGGER [dbo].[Trigger_Contact]
    ON [dbo].[Contact]
    INSTEAD OF DELETE
    AS
    BEGIN
		SET NoCount ON
        UPDATE [dbo].[Contact]
		SET [Active] = 0
		WHERE [Id] IN (SELECT [Id] FROM [deleted])
    END