﻿CREATE PROCEDURE [dbo].[AddStudent]
	@firstName VARCHAR(50),
	@lastName VARCHAR(50),
	@birthDate DATETIME,
	@yearResult INT,
	@sectionID INT = NULL
AS
	INSERT INTO [Student] ([Active], [LastName], [BirthDate], [YearResult], [SectionID]) VALUES (@firstName, @lastName, @birthDate, @yearResult, @sectionID)
RETURN 0
