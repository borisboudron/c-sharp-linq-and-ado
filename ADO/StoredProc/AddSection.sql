﻿CREATE PROCEDURE [dbo].[AddSection]
	@id INT,
	@sectionName VARCHAR(50)
AS
	INSERT INTO [Section] ([ID], [SectionName]) VALUES (@id, @sectionName)
RETURN 0
