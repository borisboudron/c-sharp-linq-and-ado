﻿CREATE PROCEDURE [dbo].[UpdateStudent]
	@yearResult INT,
	@sectionID INT
AS
	UPDATE [Student] SET [YearResult] = @yearResult, [SectionID] = @sectionID
RETURN 0
