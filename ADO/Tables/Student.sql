﻿CREATE TABLE [dbo].[Student]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] VARCHAR(50) NOT NULL, 
    [LastName] VARCHAR(50) NOT NULL, 
    [BirthDate] DATETIME2 NOT NULL, 
    [YearResult] INT NOT NULL, 
    [SectionID] INT NOT NULL, 
    [Active] BIT NOT NULL DEFAULT 1, 
    CONSTRAINT [FK_Student_Section] FOREIGN KEY ([SectionID]) REFERENCES [Section]([ID]), 
    CONSTRAINT [CK_Student_YearResult] CHECK ([YearResult] >= 0 AND [YearResult] <= 20), 
    CONSTRAINT [CK_Student_BirthDate] CHECK ([BirthDate] >= '01-01-1930')
)

GO

CREATE TRIGGER [dbo].[Trigger_Student]
    ON [dbo].[Student]
    INSTEAD OF DELETE
    AS
    BEGIN
        UPDATE [Student] SET [Active] = 0 WHERE [ID] = (SELECT [ID] FROM [deleted])
    END