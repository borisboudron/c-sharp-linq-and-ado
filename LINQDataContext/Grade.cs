﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Grade
    {
        public string GradeName { get; set; }
        public int Lower_Bound { get; set; }
        public int Upper_Bound { get; set; }

        public Grade From(Grade output)
        {
            GradeName = output.GradeName;
            Lower_Bound = output.Lower_Bound;
            Upper_Bound = output.Upper_Bound;

            return this;
        }

        public override string ToString()
        {
            return $"{GradeName, 4}{Lower_Bound, 6}{Upper_Bound, 6}";
        }
    }
}
