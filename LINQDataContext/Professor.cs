﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Professor
    {
        public int Professor_ID { get; set; }
        public string Professor_Name { get; set; }
        public string Professor_Surname { get; set; }
        public int Section_ID { get; set; }
        public int Professor_Office { get; set; }
        public string Professor_Email { get; set; }
        public DateTime Professor_HireDate { get; set; }
        public int Professor_Wage { get; set; }

        public override string ToString()
        {
            return $"{Professor_ID, 3}{Professor_Name, 12}{Professor_Surname, 12}{Section_ID, 6}{Professor_Office, 4}{Professor_Email, 30}{Professor_HireDate, 12 :dd/MM/yyyy}{Professor_Wage, 8}";
        }

        public Professor From(Professor output)
        {
            Professor_ID = output.Professor_ID;
            Professor_Email = output.Professor_Email;
            Professor_HireDate = output.Professor_HireDate;
            Professor_Name = output.Professor_Name;
            Professor_Office = output.Professor_Office;
            Professor_Surname = output.Professor_Surname;
            Professor_Wage = output.Professor_Wage;
            Section_ID = output.Section_ID;

            return this;
        }
    }
}
