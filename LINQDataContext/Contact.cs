﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Contact
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public int AnneeDeNaissance { get; set; }

        public Contact From(Contact c)
        {
            Nom = c.Nom;
            Prenom = c.Prenom;
            Email = c.Email;
            AnneeDeNaissance = c.AnneeDeNaissance;

            return this;
        }

        public override string ToString()
        {
            return $"{Nom, 14}{Prenom, 16}{Email, 36}{AnneeDeNaissance, 8}";
        }
    }
}
