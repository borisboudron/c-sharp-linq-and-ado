﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class RendezVous
    {
        public string Email { get; set; }
        public DateTime Date { get; set; }

        public RendezVous From(RendezVous output)
        {
            Email = output.Email;
            Date = output.Date;

            return this;
        }

        public override string ToString()
        {
            return $"{Email, 30}{Date, 16 :dd/MM/yyyy}";
        }
    }
}
