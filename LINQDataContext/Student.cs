﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Student
    {
        public int Student_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Login { get; set; }
        public int Section_ID { get; set; }
        public int Year_Result { get; set; }
        public string Course_ID { get; set; }

        public override string ToString()
        {
            return $"{Student_ID, 3}{First_Name, 14}{Last_Name, 18}{BirthDate, 14 :dd/MM/yyyy}{Login, 12}{Section_ID, 6}{Year_Result, 4}{Course_ID, 8}";
        }

        public Student From(Student output)
        {
            Student_ID = output.Student_ID;
            First_Name = output.First_Name;
            Last_Name = output.Last_Name;
            BirthDate = output.BirthDate;
            Login = output.Login;
            Section_ID = output.Section_ID;
            Year_Result = output.Year_Result;
            Course_ID = output.Course_ID;

            return this;
        }
    }
}
