﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Course
    {
        public string Course_ID { get; set; }
        public string Course_Name { get; set; }
        public float Course_Ects { get; set; }
        public int Professor_ID { get; set; }

        public Course From(Course output)
        {
            Course_ID = output.Course_ID;
            Course_Name = output.Course_Name;
            Course_Ects = output.Course_Ects;
            Professor_ID = output.Professor_ID;

            return this;
        }

        public override string ToString()
        {
            return $"{Course_ID, 10}{Course_Name, 50}{Course_Ects, 6}{Professor_ID, 6}";
        }
    }
}
