﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQDataContext
{
    public class Section
    {
        public int Section_ID { get; set; }
        public string Section_Name { get; set; }
        public int Delegate_ID { get; set; } //Is a Student_ID

        public Section From(Section output)
        {
            Section_ID = output.Section_ID;
            Section_Name = output.Section_Name;
            Delegate_ID = output.Delegate_ID;

            return this;
        }

        public override string ToString()
        {
            return $"{Section_ID, 6}{Section_Name, 30}{Delegate_ID, 6}";
        }
    }
}
